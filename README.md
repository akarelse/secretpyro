# Secretpyro for PyroCMS

## Legal

This module was originally written by [Aat Karelse](http://vuurrosmedia.nl)
It is licensed under the version 3 of the GNU General Public License. [GPL license](http://www.gnu.org/licenses/)
More information about this module can be found at [more information](http://modules.vuurrosmedia.nl)

---

## Overview

Secretpyro is a pluginfor PyroCMS and it supports version 2.1.x 
It's a plugin that will obfusecate html and reveal it with javascript

---

## Features

- obfusecate html with base64 and xor 'encription'

---

## Installation

Put the extracted plugin in mainpath/addons/shared_addons/plugins

---

## Ussage

{{ secretpyro:init }}

{{ secretpyro:obfuse key="123"}}

THIS WILL NOT BE SEEN BY BOTS OF BROWSERS WITHOUT JAVASCRIPT.

{{ /secretpyro:obfuse }}

This will render the text in an obfu html tag xored and base64 ed

---

## Note

This plugin will stop weak bots and perhaps stupid humans good chance it will stop spam bots from scraping email tel nr's and submitting forms.


---

## ToDo
+ perhaps other 'encryption' methods
+ some other tricks to confuse bots
+ perhaps some obfucation of the javascript
